package adventure_game.ItemBehaviour;


import adventure_game.Characters.Character;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import adventure_game.Base.Types.*;
import java.io.Serializable;

/**
 *
 * @author ted
 */
public class Inventory implements Serializable{

    public Item[] Gear;
    public Item[] Inv;
    public int curIndex;//checks item
    public int invSize;
    transient public final Item Empty = new Item();

        // Constr
    public Inventory() {
        Gear = new Item[6];
        for (int i = 0; i < 6; ++i) {
            Gear[i] = new Item();
        }
        Inv = new Item[50];
        for (int i = 0; i < 50; ++i) {
            Inv[i] = new Item();
        }

    }

    // checks if inventory is empty
    public boolean isEmpty() {
        for (int i = 0; i < this.invSize; ++i) {
            if (Inv[i].getType() == InvType.NoItem) {
                curIndex = i;
                return true;
            }
        }
        return false;
    }

    // Insert item
    public boolean insertItem(Item i) {
        if (isEmpty()) {
            this.Inv[curIndex] = i;
            System.out.println("DONE!");
            return true;
        } else {
            System.out.println("You are Full!");
        }

        return false;
    }

    // Insert item
    public void insertArmor(Character xx, Item i, List<Item> inv) {
        if (xx.getGold() >= i.getBgold() && isEmpty()) {

            for (int k = 18; k < 38; k++) {
                if (inv.get(k).getName().equals(i.getName()) && inv.get(k).getType() != InvType.Armor) {
                    for (int ind = 0; ind < this.invSize; ind++) {
                        if (Inv[ind].getType() == InvType.NoItem) {
                            Inv[ind] = inv.get(k);
                            break;
                        }

                    }
                }
            }

            System.out.print("You bought " + i.getName() + " armor!! \n");

            xx.setGold(xx.getGold() - i.getBgold());

        } else {
            System.out.print("You dont have enough gold or Your Inventory is Full\n");
        }
    }

    // Delete item
    public void delete(int index) {
        Inv[index] = Empty; // swap empty and current item
    }

    
    
    
    public void useIt(Character X, int i){
        Item it = Inv[i];
        
        if(it.getType() == InvType.Potion) {
            
            int str = X.getStrength() + it.getAttack();
            int hlt = X.getHealth() + it.getDefense();
            int spd = X.getSpeed() + it.getAgility();
            
            X.setStrength(str); // has no limit
            X.setHealth(hlt);
            X.setSpeed(spd);
            
            if(X.getHealth() > X.getMaxHealth()){
                Funcs.writeLog("[REJECT] Your Health is Full");
                X.setHealth(X.getMaxHealth());
            }
            
            if(X.getSpeed() > X.getMaxSpeed()){
                Funcs.writeLog("[REJECT] Your Speed is Full");
                X.setSpeed(X.getMaxSpeed());
            }
            

            Funcs.writeLog("[ DONE ] You got powers!!!");

            delete(i);   
        }else if(it.getType() != InvType.NoItem){
            Funcs.writeLog("[ DONE ] \n " + it.toString());
        }else{
            Funcs.writeLog("[REJECT] It has no effect!!!");
        }
        
    }


    // Move Item to Inventory
    public void MoveToInv(Character X, int G) {
        if (isEmpty()) {
            Funcs.writeLog(String.format("[ DONE ] %s %s moved to Inventory.",
                    Gear[G].getName(), Gear[G].getType().toString()));
            X.del_prop(Gear[G]);
            Inv[curIndex] = Gear[G];
            Gear[G] = Empty;

        } else {
            Funcs.writeLog("[REJECT] You Are Full !");
        }

    }

    // Move Item to Gear
    public void MoveToGear(Character X, int index) {       // Slot Empty
        List<String> weapons = Arrays.asList("Sword", "Bow", "Axe","Shield");

        if (weapons.contains(Inv[index].getType().toString())) {
            if (Gear[0].getType() == InvType.NoItem) {
                Funcs.writeLog(String.format("[ DONE ] %s %s moved to Gear.",
                        Inv[index].getName(), Inv[index].getType().toString()));
                X.prop(Inv[index]);

                Gear[0] = Inv[index];
                Inv[index] = Empty;

            } else if (Gear[1].getType() == InvType.NoItem) {
                Funcs.writeLog(String.format("[ DONE ] %s %s moved to Gear.",
                        Inv[index].getName(), Inv[index].getType().toString()));
                X.prop(Inv[index]);

                Gear[1] = Inv[index];
                Inv[index] = Empty;

            } else {
                System.out.print("Your Weapon Slots is Full! Do you want to change itmes? [ Y/n ]: ");
                if (new Scanner(System.in).nextLine().equals("Y")) {
                    Item swap;
                    
                    int slot = Funcs.Input("Choose Slot: ", 1, 6) - 1;

                    X.del_prop(Gear[slot]);
                    X.prop(Inv[index]);
                    swap = Gear[slot];
                    Gear[slot] = Inv[index];
                    Inv[index] = swap;

                    Funcs.writeLog("[ DONE ] Changed!");

                }

            }

        } else {//
            for (int x = 0; x < Gear.length; x++) {
                if (GearType.values()[x].toString().equals(Inv[index].getType().toString())) {
                    Funcs.writeLog(String.format("[ DONE ] %s %s moved to Gear.",
                            Inv[index].getName(), Inv[index].getType().toString()));
                    X.prop(Inv[index]);
                    Gear[x] = Inv[index];
                    Inv[index] = Empty;
                    return;
                }
            }

        }
    }

}//Inventory
