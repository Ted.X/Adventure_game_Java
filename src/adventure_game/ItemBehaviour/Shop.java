/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.ItemBehaviour;

import java.util.*;
import java.io.IOException;
import adventure_game.Characters.Character;
import adventure_game.Base.*;

/**
 *
 * @author ted
 */
public class Shop {

    //____________________________ S  H  O  P __________________________________
    static Scanner input = new Scanner(System.in);

    //shop table blocks (shopping)
    public static boolean shcase(Character x, Item i, Base Data)//,String n,String t)
    {
        if ("Armor".equals(i.getType().name())) {
            x.insertArmor(x, i, Data.Items());
            return true;
        }
        else if (x.getGold() >= i.getBgold()) {
            
            if (x.insertItem(i)) {
                System.out.print(String.format("You bought %s %s  !!!", i.getName(), i.getType().toString()));
                x.setGold(x.getGold() - i.getBgold());
                return true;
            }else{
                return false;
            }
        }else{
            System.out.print("You dont have enough gold!\n");
            input.nextLine();
            return false;
        }

    }

    //Create shop table
    public static void shopTable(Character c, List<Item> v) {

        Funcs.clear();
        String[] names = new String[8];
        String[] attac = new String[8];
        String[] defen = new String[8];
        String[] agili = new String[8];
        String[] types = new String[8];
        String[] cost_ = new String[8];

        int z = 0;
        for (Item n : v) {
            names[z] = String.format("  %-10s", n.getName());
            types[z] = String.format("   %-9s", n.getType());
            attac[z] = String.format(" Attack: %-3d", n.getAttack());
            defen[z] = String.format(" Defenc: %-3d", n.getDefense());
            agili[z] = String.format(" Agilit: %-3d", n.getAgility());
            cost_[z] = String.format("   Cost:%-4.4s", n.getBgold()> 1000 ? 
                                                      ((float)n.getBgold()/1000 +"k") : (" "+n.getBgold()));
            z++;
        }
        String Gold = String.format("%-8d", c.getGold());
        
        System.out.println(" ╔═══════════════════════════════════════════════════════════════════════════════╗  ");
        System.out.println(" ║ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░");
        System.out.println(" ║ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░");
        System.out.println(" ║ ░░░░░╔═════1══════╗░░░░░╔═════2══════╗░░░░░╔═════3══════╗░░░░░╔═════4══════╗░░░░░");
        System.out.println(" ║ ░░░░░║"+names[0]+"║░░░░░║"+names[1]+"║░░░░░║"+names[2]+"║░░░░░║"+names[3]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+types[0]+"║░░░░░║"+types[1]+"║░░░░░║"+types[2]+"║░░░░░║"+types[3]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+attac[0]+"║░░░░░║"+attac[1]+"║░░░░░║"+attac[2]+"║░░░░░║"+attac[3]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+defen[0]+"║░░░░░║"+defen[1]+"║░░░░░║"+defen[2]+"║░░░░░║"+defen[3]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+agili[0]+"║░░░░░║"+agili[1]+"║░░░░░║"+agili[2]+"║░░░░░║"+agili[3]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+cost_[0]+"║░░░░░║"+cost_[1]+"║░░░░░║"+cost_[2]+"║░░░░░║"+cost_[3]+"║░░░░░");
        System.out.println(" ║ ░░░░░╚════════════╝░░░░░╚════════════╝░░░░░╚════════════╝░░░░░╚════════════╝░░░░░");
        System.out.println(" ║ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░");
        System.out.println(" ║ ░░░░░░░░░░░░░░░░░░░░░░❯❯❯❯❯❯❯❯ CHOOSE WHICH U WANT❮❮❮❮❮❮❮❮░░░░░░░░░░░░░░░░░░░░░░░");
        System.out.println(" ║ ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░");
        System.out.println(" ║ ░░░░░╔═════5══════╗░░░░░╔═════6══════╗░░░░░╔═════7══════╗░░░░░╔═════8══════╗░░░░░");
        System.out.println(" ║ ░░░░░║"+names[4]+"║░░░░░║"+names[5]+"║░░░░░║"+names[6]+"║░░░░░║"+names[7]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+types[4]+"║░░░░░║"+types[5]+"║░░░░░║"+types[6]+"║░░░░░║"+types[7]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+attac[4]+"║░░░░░║"+attac[5]+"║░░░░░║"+attac[6]+"║░░░░░║"+attac[7]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+defen[4]+"║░░░░░║"+defen[5]+"║░░░░░║"+defen[6]+"║░░░░░║"+defen[7]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+agili[4]+"║░░░░░║"+agili[5]+"║░░░░░║"+agili[6]+"║░░░░░║"+agili[7]+"║░░░░░");
        System.out.println(" ║ ░░░░░║"+cost_[4]+"║░░░░░║"+cost_[5]+"║░░░░░║"+cost_[6]+"║░░░░░║"+cost_[7]+"║░░░░░");
        System.out.println(" ║ ░░░░░╚════════════╝░░░░░╚════════════╝░░░░░╚════════════╝░░░░░╚════════════╝░░░░░");
        System.out.println(" ╚═░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░");
        System.out.println("   ░░░░░░░░░░░░░░ Gold: "+Gold+"░░░░░░░░░░░░░░░░░░ PRESS 9 TO EXIT SHOP ░░░░░░░░░░░░ \n");
    }

    // S H O P
    public static void shop(Character Char, Base Data) throws IOException {
                
        List<Item> shopItems = Data.Loc().get(Base.curLoc).getShopItems();

        if(shopItems == null){
            System.out.println("Shop is Empty");
            input.nextLine();
            return;
        }
        while (true) {
            shopTable(Char, shopItems);
            int p = Funcs.Input("Enter Number: ", 1, 9);
            if (p == 9) 
                return;

            if(shcase(Char, shopItems.get(p - 1), Data)){
                input.nextLine();
            }
        }

    }

}
