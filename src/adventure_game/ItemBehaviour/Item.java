package adventure_game.ItemBehaviour;

import adventure_game.Base.Types.*;
import java.io.Serializable;

public class Item implements Serializable{

    private String name;
    private InvType type;
    private int attack;
    private int defense;
    private int agility;

    private int Bgold; // Item Buy Price
    private int Sgold; // Item sell Price

    // Constructors
    public Item() {
        name = "empty";
        type = InvType.NoItem;
        attack = 0;
        defense = 0;
        agility = 0;
        Bgold = 0;
        Sgold = 0;
    }

    public Item(String... args) {
        this.name = args[0];
        this.type = InvType.valueOf(args[1]);
        this.attack = Integer.parseInt(args[2]);
        this.defense = Integer.parseInt(args[3]);
        this.agility = Integer.parseInt(args[4]);
        this.Bgold = Integer.parseInt(args[5]);
        this.Sgold = Integer.parseInt(args[6]);
    }


    @Override
    public String toString(){
        return String.format("Name: %s %s \n Attack:   %4d \n Defense:  %4d \n Agility:  %4d \n BuyGold:  %4d \n SellGold: %4d \n ",
                            this.name, this.type, this.attack, this.defense, this.agility, this.Bgold, this.Sgold);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the type
     */
    public InvType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(InvType type) {
        this.type = type;
    }

    /**
     * @return the attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * @param attack the attack to set
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * @return the defense
     */
    public int getDefense() {
        return defense;
    }

    /**
     * @param defense the defense to set
     */
    public void setDefense(int defense) {
        this.defense = defense;
    }

    /**
     * @return the agility
     */
    public int getAgility() {
        return agility;
    }

    /**
     * @param agility the agility to set
     */
    public void setAgility(int agility) {
        this.agility = agility;
    }

    /**
     * @return the Bgold
     */
    public int getBgold() {
        return Bgold;
    }

    /**
     * @param Bgold the Bgold to set
     */
    public void setBgold(int Bgold) {
        this.Bgold = Bgold;
    }

    /**
     * @return the Sgold
     */
    public int getSgold() {
        return Sgold;
    }

    /**
     * @param Sgold the Sgold to set
     */
    public void setSgold(int Sgold) {
        this.Sgold = Sgold;
    }

}
