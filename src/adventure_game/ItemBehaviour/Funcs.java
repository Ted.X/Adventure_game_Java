package adventure_game.ItemBehaviour;

import adventure_game.Base.*;
import adventure_game.Characters.Character;
import adventure_game.Main.Crypt;
import adventure_game.QuestBehaviour.*;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.*;

public class Funcs {

    private final static Random rand = new Random();
    private final static Scanner input = new Scanner(System.in);

    /**
     * Prints Adventure Game ASCII Picture :D
     */
    public static void Intro() {
        try {
            Funcs.clear();
            //Path filepath = Paths.get("Data/Intro.dat");
            //List<String> lines = Files.readAllLines(filepath, Charset.defaultCharset());
            List<String> lines = new Crypt().decode("Data/Intro.dat");
            for (String line : lines) {
                System.out.println(line);
                System.out.flush();
                Thread.sleep(20);
            }
            Thread.sleep(1000);
            System.out.println("\n\n\t\t\t < Press Enter To Start... >");
            input.nextLine();
            
            System.out.println("\t\t\t\t1.New Game\n");
            System.out.println("\t\t\t\t2.Load Game\n");
            System.out.println("\t\t\t\t3.Exit \n");
            System.out.print("\t\t\t     ");
            
        } catch (IOException | InterruptedException e) {
            System.out.println("Intro error" + e);
        }
    }

    /**
     * Insert String into Box
     *
     * @param string String
     * @param boxSize int
     * @return List of Strings
     */
    public static List<String> insertString(String string, int boxSize) {
        //StringBuilder str= new StringBuilder();
        String[] words = string.split(" ");

        List<String> box = new ArrayList<>();
        box.add("");
        int cc = boxSize;
        int h = 0;

        for (String st : words) {
            if (st.length() < boxSize) {  // if there is space
                boxSize -= st.length()+1;
                box.set(h, box.get(h).concat(st+" "));
                if (st.equals(words[words.length - 1])) {  // if last string close line

                    for (int i = 0; i < boxSize; i++) {
                        box.set(h, box.get(h).concat(" "));
                    }

                    h += 1;
                }
            } else {  // if is not, move to new line
                for (int i = 0; i < boxSize; i++) {
                    box.set(h, box.get(h).concat(" "));
                }

                h += 1;
                boxSize = cc;
                box.add("");  // start new line
                if (st.equals(words[words.length - 1])) {
                    box.set(h, box.get(h).concat(st));
                    for (int i = 0; i < (cc - st.length()); i++) {
                        box.set(h, box.get(h).concat(" "));
                    }
                    h += 1;
                } else {
                    boxSize -= st.length()+1;
                    box.set(h, box.get(h).concat(st+" "));
                }
            }
        }
        return box;
    }

    /**
     * Print like chat clouds
     *
     * Wizard: 
     * ┌───────────────────────────────────────┐ 
     * | Where have you been???                     ││- - - - - - - - - - - - -
     * └────────────────────────────────────────────┘│- - - - - - - - - - - - -
     *   ────────────────────────────────────────────┘- - - - - - - - - - - - -
     *
     * @param string
     * @param name
     * @param x
     * @param y
     */
    public static void printCloud(String string, String name, int x, int y) {

        int cloudSize = rand.nextInt(y+1 - x) + x;
        
        String sep = "";
        for (int i = cloudSize+5; i < 50; i+=2) {
            sep += " -";
        }

        List<String> Bubble = insertString(string, cloudSize - 2); // insert string in bubble

        String space = "";
        for (int i = 0; i < cloudSize; i++) {
            space += "─";
        }

        // Now print
        if (name.equals("Me")) {
            String tab = sep.replace("-", " ");
            
            System.out.println(tab + name + ":  ");

            System.out.println(tab + " ┌" + space + "┐");  // print first 2 Lines

            for (String w : Bubble) {
                System.out.println(sep + "││ " + w + " │");
            }
            System.out.println(sep + "│└"+space+"┘");
            System.out.println(sep + "└"+space+"\n\n\n");
            input.nextLine();
        } else {
            System.out.println("  "+name+":");
            System.out.println("  ┌"+space+"┐");  // print first 2 Lines
            for (String w : Bubble) {
                System.out.println("  │ "+w+" ││" + sep);
            }
            System.out.println("  └"+space+"┘│" + sep);
            System.out.println("    "+space+"┘" + sep + "\n\n\n");
            input.nextLine();
        }
    }

    /**
     * Clears Console
     */
    public static void clear() {
        try {
            if (System.getProperty("os.name").equals("nt")) // if Windows
            {
                Runtime.getRuntime().exec("cls");
            } else // Other OS
            {
                System.out.print("\033[H\033[2J");
            }
        } catch (IOException e) {
            System.out.println("Cant clear screen" + e.getMessage());
        }

    }

    /**
     * Input for numbers
     *
     * @param in_string
     * @return Number
     */
    public static int Input(String in_string) {
        /* Input for numbers */
        String out = "";
        String X = "";
        if (in_string != null) {
            out = in_string;
        }

        Scanner input = new Scanner(System.in);
        while (true) {
            try {
                System.out.print(out);
                X = input.nextLine();
                return Integer.parseInt(X);
            } catch (NumberFormatException e) {
                if (X.equals("quit") || X.equals("back")) {
                    return 0;
                } else {
                    System.out.println("I said Number!");
                }

            }
        }
    }

    /**
     * Input for numbers (in range)
     *
     * @param in_string
     * @param left
     * @param right
     * @return Number
     */
    public static int Input(String in_string, int left, int right) {
        String out = "";
        String X = "";
        if (in_string != null) {
            out = in_string;
        }

        Scanner input = new Scanner(System.in);
        while (true) {
            try {
                System.out.print(out);
                X = input.nextLine();
                int num = Integer.parseInt(X);
                if (num < left || num > right) {
                    throw new Throwable();
                }
                return num;
            } catch (NumberFormatException e) {
                if (X.equals("quit") || X.equals("back")) {
                    return 0;
                } else {
                    System.out.println("I said Number!");
                }

            } catch (Throwable e) {
                System.out.println("Input Correct Prompt!!!");
            }
        }
    }

    public static void writeLog(String l) {
        Base.logs.add("\b------------------------------");
        if (l.length() < 30) {
            Base.logs.add(l);
        } else {
            String[] z = l.split(" ");
            String sub = "";
            int i = 0;
            while (true) {
                boolean c = true;
                for (; i < z.length; i++) {
                    if (c && (sub+z[i]).length() < 27) {
                        if (z[i].equals("\n")) {
                            c = false;
                        } else {
                            sub += z[i]+" ";
                        }
                    } else {
                        break;
                    }
                }
                Base.logs.add(sub);
                if (i < z.length) {
                    sub = "";
                } else {
                    break;
                }
            }
            //Base.logs.add(l.substring(sub.length()));
        }

    }

    public static void dayCounter(Character Char, int day, int hour, int min) {
        DateTime p = Char.getTime();
        p = p.plusDays(day);
        p = p.plusHours(hour);
        p = p.plusMinutes(min);

        Char.setTime(p);
    }

    // Drop Item
    public static void dropper(Character Char, List<Item> items) {
        Map<Integer, List<Item>> Awards = new HashMap<>();
        for (int i = 0; i < 20; i += 5) {
            Awards.put(i, new ArrayList<Item>());
            Awards.get(i).add(Char.Empty);
        }

        for (Item x : items) {
            switch (x.getName()) {
                case "Wooden":
                    Awards.get(0).add(x);
                    break;
                case "Iron":
                    Awards.get(5).add(x);
                    break;
                case "Golden":
                    Awards.get(10).add(x);
                    break;
                case "Dragon":
                    Awards.get(15).add(x);
                    break;
            }
        }

        List<Integer> list = new ArrayList<>(Awards.keySet());
        Collections.reverse(list);

        for (int l : list) {
            if (Char.getLvl() >= l) {
                Item it = Awards.get(l).get(rand.nextInt(Awards.get(l).size()));
                if (!it.getName().equals("empty")) {
                    Char.insertItem(it);
                    Char.prop(it);
                    System.out.println("Dropped "+it.getName()+" "+it.getType());
                    input.nextLine();
                }
                return;
            }
        }

    }


    public static void messages(Character Char, Base Data) {
        clear();
        if (Data.Locind(Base.curLoc).isFirstTime()) {   // Do everything for the first time here
            System.out.println(String.format("Welcome, %s to %s !", Char.getName(), Data.Locind(Base.curLoc).getName()));
            Data.Locind(Base.curLoc).setFirstTime(false);

        } else {
            clear();

            if (!Data.Msgs().isEmpty()) {
                clear();
                System.out.println(Data.Msgs(rand.nextInt(Data.Msgs().size())));   // display :>
            } else {
                System.out.println("Welcome back, "+Char.getName()+"!");
            }

            System.out.println("\n");
        }
    }


    // Print character's information for battle
    public static void printChars(List<Character> ch) {
        int len = ch.get(0).getInfo().length;
        int maxChar = 4;
        for (Character ch1 : ch) {
            ch1.updateInfo();
        }
        //ch.add(new Characters.Elf());

        List<Character> chars = ch;
        if (ch.size() > maxChar) {
            chars = ch.subList(0, maxChar);
            ch = ch.subList(maxChar, ch.size());
        }else{
            ch = null;
        }

        for (int i=0; i < len; i++) {
            for (Character char1 : chars) {
                if (i == 0 || i == len - 1) {
                    System.out.print(char1.getInfo()[i]+"+");
                } else {
                    System.out.print(char1.getInfo()[i]+"|");
                }
            }

            System.out.println();
        }
        if (ch != null) {
            printChars(ch);
        }
    }

    // Create Inventory table
    public static void invTable(Character c) throws NumberFormatException {


        while (true) {
            clear();
            
            List<String> table = new ArrayList<>();

            String[] names = new String[6];
            String[] attac = new String[6];
            String[] defen = new String[6];
            String[] agili = new String[6];
            String[] types = new String[6];

            int z = 0;
            for (Item n : c.Gear) {
                names[z] = String.format("  %-10s", n.getName());
                types[z] = String.format("   %-9s", n.getType());
                attac[z] = String.format(" Attack: %-3d", n.getAttack());
                defen[z] = String.format(" Defenc: %-3d", n.getDefense());
                agili[z] = String.format(" Agilit: %-3d", n.getAgility());
                z++;
            }
            String Gold = String.format("%-8d", c.getGold());

            String weapon1_ = (c.Gear[0].getType() == Types.InvType.NoItem) ? "  Weapon1   " : types[0];
            String weapon2_ = (c.Gear[1].getType() == Types.InvType.NoItem) ? "  Weapon2   " : types[1];

            table.add(" ╔═══════════════════════════════════════════════════════════════════════════╗  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒ Gold:"+Gold+"▒▒▒▒▒▒▒▒▒▒▒   W E A P O N S   ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╔═════1══════╗▒▒▒▒▒▒▒▒▒▒▒▒╔═════2══════╗▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║"+names[0]+"║▒▒▒▒▒▒▒▒▒▒▒▒║"+names[1]+"║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║"+weapon1_+"║▒▒▒▒▒▒▒▒▒▒▒▒║"+weapon2_+"║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║"+attac[0]+"║▒▒▒▒▒▒▒▒▒▒▒▒║"+attac[1]+"║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║"+defen[0]+"║▒▒▒▒▒▒▒▒▒▒▒▒║"+defen[1]+"║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║"+agili[0]+"║▒▒▒▒▒▒▒▒▒▒▒▒║"+agili[1]+"║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒╚════════════╝▒▒▒▒▒▒▒▒▒▒▒▒╚════════════╝▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒     A R M O R     ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒╔═════3══════╗▒▒▒▒╔═════4══════╗▒▒▒▒╔═════5══════╗▒▒▒▒╔═════6══════╗▒▒▒▒ ║  ");
            table.add("▒▒▒▒║"+names[2]+"║▒▒▒▒║"+names[3]+"║▒▒▒▒║"+names[4]+"║▒▒▒▒║"+names[5]+"║▒▒▒▒ ║  ");
            table.add("▒▒▒▒║   Head     ║▒▒▒▒║   Chest    ║▒▒▒▒║   Pants    ║▒▒▒▒║   Boots    ║▒▒▒▒ ║  ");
            table.add("▒▒▒▒║"+attac[2]+"║▒▒▒▒║"+attac[3]+"║▒▒▒▒║"+attac[4]+"║▒▒▒▒║"+attac[5]+"║▒▒▒▒ ║  ");
            table.add("▒▒▒▒║"+defen[2]+"║▒▒▒▒║"+defen[3]+"║▒▒▒▒║"+defen[4]+"║▒▒▒▒║"+defen[5]+"║▒▒▒▒ ║  ");
            table.add("▒▒▒▒║"+agili[2]+"║▒▒▒▒║"+agili[3]+"║▒▒▒▒║"+agili[4]+"║▒▒▒▒║"+agili[5]+"║▒▒▒▒ ║  ");
            table.add("▒▒▒▒╚════════════╝▒▒▒▒╚════════════╝▒▒▒▒╚════════════╝▒▒▒▒╚════════════╝▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ║  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒ ╝  ");
            table.add("▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒    ");

            while (Base.logs.size() > 23) {
                Base.logs.remove(0);
            }

            List<String> logTable = new ArrayList<>();

            logTable.add(0, ".==============================.");
            for (String x : Base.logs) {
                String line = String.format("| %-29s|", x);
                logTable.add(line);
            }
            logTable.add("`==============================`");

            for (int i = 0; i < 25; i++) {
                System.out.println(table.get(i)+logTable.get(i));
            }

            int col = 0;
            int it = 1;
            System.out.println("╔═════════════════════════════════════════════════════════════════════════════════════════════╗");
            for (int i = 0; i < c.invSize; i++) {
                if (c.Inv[i].getName().equals("empty")) {
                    System.out.print("║      empty      ║");
                } else {
                    System.out.print(String.format("║ %-7s %-7s ║",// At:%-3s Df:%-3s Ag:%-3s ║",
                            c.Inv[i].getName(), c.Inv[i].getType()));
                }

                col++;
                if (col % 5 == 0) {
                    System.out.println(String.format("(%d-%d)", it, it+4));
                    it += 5;
                } else if (i == c.invSize - 1 && c.invSize % 5 != 0) {
                    int empt = 5 - c.invSize % 5;
                    while (empt != 0) {
                        System.out.print("║▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒║");
                        empt--;
                    }
                    System.out.println("("+it+"-"+(it+4)+")");
                    it += 5;
                }
            }

            System.out.println("╚═════════════════════════════════════════════════════════════════════════════════════════════╝");

            System.out.println("JUST DO IT!!!!");
            System.out.println("┌──────────────────────┬──────────────────────┬──────────────────────┐");
            System.out.println("│1. Use Item           │ 2. Move to Inventory │ 3. Move to Gear      │");
            System.out.println("╞──────────────────────╪──────────────────────╪──────────────────────╡");
            System.out.println("│4. Sell Item          │ 5. Gold -> XP        │ 6. EXIT              │");
            System.out.println("└──────────────────────┴──────────────────────┴──────────────────────┘");

            int inv_f = Input(">> ", 1, 6);

            switch (inv_f) {
                case 1: // USE IT
                    int p = Input("\nChoose the Index of Item: ", 1, c.invSize);
                    c.useIt(c, p - 1);
                    break;
                case 2:
                    int MTI = Input("\nChoose the Slot: ", 1, 6);
                    c.MoveToInv(c, MTI - 1);
                    break;
                case 3:
                    int MTG = Input("\nChoose the Index of Item: ", 1, c.invSize);
                    c.MoveToGear(c, MTG - 1);
                    break;
                case 4:
                    int itm = Input("\nChoose the Index of Item: ", 1, c.invSize) - 1;
                    c.setGold(c.getGold()+c.Inv[itm].getSgold());
                    writeLog("[ DONE ] You earned "+c.Inv[itm].getSgold()+" Gold.");
                    c.delete(itm);
                    break;
                case 5:
                    int gold = Math.abs(Input("\nInput Gold: "));
                    if (gold <= c.getGold()) {
                        c.goldToxp(gold);
                    } else {
                        writeLog("[REJECT] You don't have enough money!");
                    }
                    break;
                case 6:
                    return;
            }

        }

    }

    public static String mapFormat(Object[] args) {
        try {
            //byte[] MapString = Files.readAllBytes(Paths.get("Data/Map.dat"));
            String wholeString= "";//= new String(MapString, Charset.defaultCharset());
            List<String> strings = new Crypt().decode("Data/Map.dat");
            for(String x: strings)
                wholeString += x + "\n";

            return String.format(wholeString, (Object[]) args);

        } catch (IOException ex) {
            System.out.println("Cant format map file");
        }
        return null;

    }

    // Create Map
    public static void map(List<Location> dat) {
        
        /********************
        0. Nemertea  =  home town
        1. Flaming Stones
        2. Wizard Tower
        3. Shadow Mountain
        4. Golden Forest
        5. Dead Lake
        6. Orc's Stronghold
        7. Hjortar Hus
        8. Black Fall
        9. Final BossAssNigga
        *********************/

        String[] l = new String[10];

        for (int i = 0; i < l.length; i++) {
            l[i] = "    ";
        }

        l[Base.curLoc] = "You ";

        String[] X = new String[dat.size()];

        for (int i = 0; i < dat.size(); i++) {
            if (dat.get(i).isLocked())//is true
            {
                X[i] = "X";
            } else {
                X[i] = ".";
            }
        }

        String[] leg = new String[10];

        for (int ind = 0; ind < dat.size(); ind++) {
            if (X[ind].equals(".")) {
                leg[ind] = String.format("%s", dat.get(ind).getName()).replace("_", " ");
            } else {
                if (ind == 2 || ind == 5) {
                    leg[ind] = "              ";
                } else {
                    leg[ind] = "                  ";
                }
            }
        }

        System.out.println();
        for (int i = 0; i < 101; ++i) {
            try {
                System.out.print(String.format("\r Checking %s %% ", i));
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                System.out.println("Loading Problem.");
            }
        }

        List<String> flags = new ArrayList<>();
        flags.addAll(Arrays.asList(l));
        flags.addAll(Arrays.asList(leg));
        flags.addAll(Arrays.asList(X));

        System.out.println(mapFormat(flags.toArray()));

    }

    public static void profile(Character Char, Base data) {
        //Prints Character's profile Menu
        Story.printStory(data, Char);
        while (true) {

            clear();
            Char.updateInfo(); // update character's information
            Char.printInfo();
            System.out.println("\n");

            System.out.println("1. Inventory ");
            System.out.println("2. Map ");
            System.out.println("3. Quests");
            System.out.println("4. Exit ");

            int in_ = Input(">> ", 1, 4);

            switch (in_) {
                case 1:
                    invTable(Char);
                    break;
                case 2:
                    map(data.Loc());
                    System.out.println("\nType \"Quit\" to go back!");
                    String invController = "";
                    while (!"quit".equals(invController)) {
                        //  get tolower and check
                        System.out.print(">> ");
                        invController = input.nextLine();
                        invController = invController.toLowerCase();
                    }
                    Funcs.dayCounter(Char, 0, 0, 10);
                    break;
                case 3:
                    Quest.update(data, Char);
                    Quest.QuestsInfo(Char, data);
                    Funcs.dayCounter(Char, 0, 0, 15);
                    break;
                case 4:
                    return;

            }
        }
    }

}
