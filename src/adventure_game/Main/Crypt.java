/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.Main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ted
 */
public class Crypt {

    private final String chars;
    private final List<java.lang.Character> Alpha;
    private String key;

    public Crypt() {
        chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
              + "*0123456789!\"\'%@#$%^&()?_+=/|<>:}{][\\,-~█▒╚╝═║╔╗╠╩╦╬╣┌┐─│└┘╪╫╞╡`.";
        
        Alpha = new ArrayList<>();
        for (int i = 0; i < chars.length(); i++) {
            Alpha.add(chars.charAt(i));
        }
        key = "a"; // roca gasagebi aris "a" cryptis methodebi chveulebriv kitxuloben filedan informacias
    }

    public String getKey() {
        return key;
    }

    public void setKey(String k) {
        this.key = k;
    }

    public List<String> encode(String path) throws IOException {

        List<String> text = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
        List<String> encoded = new ArrayList<>();

        int k = 0;

        for (String string : text) {
            String s = "";
            for (int c = 0; c < string.length(); c++) {
                int c1 = Alpha.indexOf(string.charAt(c)) - Alpha.indexOf(key.charAt(k)) % chars.length();
                if (c1 < 0) {
                    c1 = chars.length() + c1;
                }

                k += 1;
                s = s.concat(Alpha.get(c1).toString());
                if (k >= key.length()) {
                    k = 0;
                }
            }
            encoded.add(s);

        }
        return encoded;

    }

    public List<String> decode(String path) throws IOException,ArrayIndexOutOfBoundsException {

        List<String> text = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
        List<String> decoded = new ArrayList<>();

        int k = 0;
        for (String string : text) {
            String s = "";
            for (int c = 0; c < string.length(); c++) {
                int c1 = Alpha.indexOf(string.charAt(c)) + Alpha.indexOf(key.charAt(k)) % chars.length();
                if (c1 > 0) {
                    c1 = c1 % chars.length();
                }
                s = s.concat(Alpha.get(c1).toString());
                k += 1;

                if (k >= key.length()) {
                    k = 0;
                }
            }
            decoded.add(s);
        }
        return decoded;

    }

    public static void main(String[] args) {
        try {
            if (args[0].equals("-h") || args[0].equals("-help")) {
                throw new ArrayIndexOutOfBoundsException();
            }
            Crypt c = new Crypt();
            Scanner scan = new Scanner(System.in);
            List<String> pipe = null;
            String savefile;
            c.setKey(args[2]);

            if (args[1].contains(".plr")) {
                System.out.println("Cant convert byte file");
            } else if(args[1].charAt(args[1].length()-1) == '/'){ // encode/decode all files example: Data/
                File f = new File(args[1]);                
                for(String path: f.list()){
                    switch(args[0]){
                        case "-encode":
                            pipe = c.encode(args[1] + path);
                            break;
                        case "-decode":
                            pipe = c.decode(args[1] + path);
                            break;
                    }
                    try (FileWriter writer = new FileWriter(args[1] + path)) {
                        for(String str: pipe)
                            writer.write(str + "\n");
                    }
                }
                System.out.println("Done");
                        
                        
            } else {

                switch (args[0]) {
                    case "-encode":
                        pipe = c.encode(args[1]);
                        break;
                    case "-decode":
                        pipe = c.decode(args[1]);
                        break;
                }

                System.out.print("Input Save File Path(type 'same'): ");
                savefile = scan.nextLine();
                if (savefile.equals("same")) {
                    savefile = args[1];
                }

                try (FileWriter writer = new FileWriter(savefile)) {
                    for (String str : pipe) {
                        writer.write(str + "\n");
                    }
                    System.out.println("Done!");
                }
            }

        } catch (IOException ex) {
            System.out.println("No such File!!!" + ex);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("-encode <PATH> <KEY>  to encode file");
            System.out.println("-decode <PATH> <KEY>  to decode file");
        }
    }
}
