package adventure_game.Main;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import adventure_game.Base.Base;
import adventure_game.Characters.Character;
import adventure_game.Characters.Human;
import adventure_game.Characters.Elf;
import adventure_game.Characters.Orc;
import adventure_game.ItemBehaviour.Funcs;
import adventure_game.QuestBehaviour.Story;


/**
 *
 * @author ted
 */
public class AdventureGame {

    /**
     *
     * @param args
     */

    public static void main(String[] args){
        String name="";// choose name
        String choice="";// choose race

        // CREATE BASE
        Base Data = new Base();
        try {
            Data.inputObjects();
            Data.inputLoc();
            Data.inputMenus();
            Data.inputMessages();
            Data.inputQuests();
            //Data.inputQuestsAwards();
            Data.inputStory();

        } catch (ArrayIndexOutOfBoundsException d){
            System.out.println("Cant decode File");
            System.exit(0);
        } catch (FileNotFoundException e) {
            System.out.println("No such File!!!" + e);
            System.exit(0);
        } catch (IOException ex) {
            System.out.println("Cant read" + ex);
            System.exit(0);
        } 

        Scanner input = new Scanner(System.in);


        //=========CREATE CHARACTERS================
        List<Character> plr = new ArrayList<>();

        Human A = new Human();
        Orc B = new Orc();
        Elf C = new Elf();

        plr.add(A);
        plr.add(B);
        plr.add(C);

        Data.setChars(plr);

        //==========================================

        Funcs.clear();

        //_+_+_+_+_+_+_+_+_+_+_+_ I N T R O D U C T I O N _+_+_+_+_+_+_+_+_+_

        while(true) {
            Funcs.Intro();
            int nlx = Funcs.Input(">> ", 1, 3);


            Character[] heroes = new Character[]{new Human(), new Orc(), new Elf()};
            
            
            switch (nlx) {
                case 1://New Game
                    
                    Funcs.clear();
                    Story.printStory(Data, heroes[0]); // heroes[0] default character
                    
                    String n = "";
                    while (name.length() > 10 || n.equals("") ||
                            "human".equals(n) || "orc".equals(n) || "elf".equals(n)) {
                        System.out.print("Insert Name (MAX.10 symb): ");
                        name = input.nextLine();
                        n = name.toLowerCase();
                    }
                    while (!(choice.equals("human") || choice.equals("orc") || choice.equals("elf"))) {
                        System.out.print("Choose Your Character (Human,Orc,Elf): ");
                        choice = input.nextLine().toLowerCase();
                    }
                    Story.printStory(Data, heroes[0], name);
                    input.nextLine();

                    

                    //**************DEFAULT INVENTORY***********
                    for(Character ch: heroes){
                        if(ch.getClass().getSimpleName().toLowerCase().equals(choice)){
                            ch.setDefaultProperties(Data, name);
                            Data.Chars().add(ch);
                            break;
                        }
                    }
                    if(Data.Chars().size() == 3){
                        System.out.println("Cant Create Character! \n");
                        return;
                    }
                    
                    Base.MENU(Data);
                    break;
                    
                case 2://Load Game
                    Data.Load();
                    break;
                    
                default:
                    return;

            }
        }
    }
}
