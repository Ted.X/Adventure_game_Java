/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.Base;


import adventure_game.ItemBehaviour.*;
import adventure_game.QuestBehaviour.*;
import adventure_game.Main.Crypt;
import adventure_game.Characters.Character;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

//import static adventure_game.Funcs.read;

public final class Base {

    private List<Character> chars;//Characters
    private List<Item> items;//Items
    private List<Location> loc;//Locations
    private List<String> msgs;//messages
    private List<Quest> Quests;//Quests
    private List<Story> story; //StoryLine
    
    public static List<String> logs = new ArrayList<>();

    public Base() {
        chars = new ArrayList<>();
        items = new ArrayList<>();
        loc = new ArrayList<>();
        msgs = new ArrayList<>();
        Quests = new ArrayList<>();
        story = new ArrayList<>();
        
        for (int i = 0; i < 28; i++) {
            logs.add(" ");// Fill with whitespaces
        }
    }
    
    
    
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    /**
     * @return the chars
     */
    public List<Character> Chars() {
        return chars;
    }

    /**
     * @param chars the chars to set
     */
    public void setChars(List<Character> chars) {
        this.chars = chars;
    }

    /**
     * @return the items
     */
    public List<Item> Items() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     * @return the location
     */
    public List<Location> Loc() {
        return loc;
    }

    /**
     * @param i index of location
     * @return the location
     */
    public Location Locind(int i) {
        return loc.get(i);
    }

    /**
     * @return the Size of Locations
     */
    public int LocSize() {
        return loc.size();
    }

    /**
     * @param loc the location to set
     */
    public void setLoc(List<Location> loc) {
        this.loc = loc;
    }

    /**
     * @return the messages
     */
    public List<String> Msgs() {
        return msgs;
    }

    /**
     * @param i index
     * @return the message
     */
    public String Msgs(int i) {
        return msgs.get(i);
    }

    /**
     * @param msgs the messages to set
     */
    public void setMsgs(List<String> msgs) {
        this.msgs = msgs;
    }

    /**
     * @return the Quests
     */
    public List<Quest> Quests() {
        return Quests;
    }

    /**
     * @param Quests the Quests to set
     */
    public void setQuests(List<Quest> Quests) {
        this.Quests = Quests;
    }

    public List<Story> getStory() {
        return story;
    }

    public void setStory(List<Story> story) {
        this.story = story;
    }
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%END

    

    //+++++++++++++++++++++++++++++++ B A S E    M E T H O D S ++++++++++++++++++++++++++
    Random rand = new Random();
    Crypt code = new Crypt();  // to decode files

    private final static Scanner input = new Scanner(System.in);
    public static int curLoc = 0;  // CURRENT LOCATION

    /**
     * Input Objects From File {NAME}{TYPE}{ATTACK}{DEFENSE}{AGILITY}{BUY PRICE}{SELL PRICE}
     * example: 
     *          Wooden,Sword,4,0,0,150,50
     *          Iron,Sword,4,4,0,300,100
     *
     * @throws IOException
     */
    public void inputObjects() throws IOException {
        
        List<String> lines = code.decode("Data/objects.dat");
        for (String line : lines) {
            if (!line.contains("*")) { // line containing "*" is comment
                String[] lineSplit = line.split(",");
                Item stuff = new Item(lineSplit);

                this.items.add(stuff);
            }
        }
    }

    /**
     * {LOCATION NAME}
     * {Methods names}
     * TAG: @Shop Items -> must be 8 items
     *      {Item name},{Item type}
     * # === # doing nothing just separate
     *
     * @throws IOException
     */
    public void inputLoc() throws IOException {
//        try (InputStream fis = new FileInputStream("Data/Locations.dat");
//            InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
//            BufferedReader br = new BufferedReader(isr))
//        {
        List<String> lines = code.decode("Data/Locations.dat");

        List<Method> functions = new LinkedList<>();
        functions.addAll(Arrays.asList(Location.class.getDeclaredMethods()));
        functions.addAll(Arrays.asList(Funcs.class.getDeclaredMethods()));
        functions.addAll(Arrays.asList(Battle.class.getDeclaredMethods()));
        functions.addAll(Arrays.asList(Shop.class.getDeclaredMethods()));
        functions.addAll(Arrays.asList(Quest.class.getDeclaredMethods()));
        functions.addAll(Arrays.asList(Base.class.getDeclaredMethods()));


        Iterator<String> it = lines.iterator();
        while (it.hasNext() && !it.next().equals("#ENDOFLOCATIONS")) {    
            int price;
            String locName;
            List<Item> shopItems = null;

            locName = it.next();

            if(locName.contains("*"))
                continue;

            //price = Integer.parseInt(it.next());

            String[] menuStrings = it.next().split(",");
            List<Method> funcs = new ArrayList<>();

            for (String f : menuStrings) {
                for (Method m : functions) {
                    if (m.getName().equals(f)) {
                        funcs.add(m);
                        break;
                    }
                }
            }


            if(it.next().equals("@Shop Items")){
                shopItems = new ArrayList<>();
                for(int i = 0; i < 8; i++){
                    String[] lineSplit = it.next().split(",");
                    for (Item t : this.Items()) {
                        if (t.getName().equals(lineSplit[0]) && t.getType().toString().equals(lineSplit[1])) {
                            shopItems.add(t);
                            break;
                        }
                    }
                }
                if(shopItems.size() != 8){
                    throw new IOException("Not enough Items");
                }
            } 
            Location loc = new Location(locName, funcs, shopItems);
            this.Loc().add(loc);
        }
    }
    
    /**
     * use tags: @Date, @Location, @Story, @Quest
     * @throws IOException
     */
    public void inputStory() throws IOException {
        //List<String> lines = Files.readAllLines(Paths.get("Data/Story.dat"), Charset.defaultCharset());
        List<String> lines = code.decode("Data/Story.dat");
        
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy.MM.dd");
        String endOfstory = "==============================";
        String line = endOfstory;

        Iterator<String> iterator = lines.iterator();

        while (iterator.hasNext() && line.equals(endOfstory)) {
            Story s = new Story();
            line = iterator.next();

            while (true) {
                switch (line) {
                    case "@Date":
                        String ln = iterator.next();
                        DateTime time = formatter.parseDateTime(ln);
                        s.setDate(time);
                        break;
                    case "@Location":
                        String loc = iterator.next();
                        for (Location l : this.Loc()) {
                            if (l.getName().equals(loc)) {
                                s.setLoc(l.getName());
                                break;
                            }
                        }
                        break;
                    case "@Quest":
                        String qst = iterator.next();
                        for (Quest q : this.Quests()) {
                            if (q.getName().equals(qst)) {
                                s.setQuest(q.getName());
                                break;
                            }
                        }
                        break;
                    case "@Story":
                        String storyLine = iterator.next();
                        if (storyLine.equals(endOfstory)) {
                            line = storyLine;
                            break;
                        } else {
                            s.getStoryLine().add(storyLine);
                            continue;
                        }
                    default:
                        break;

                }

                if (line.equals(endOfstory)) {
                    break;
                }

                line = iterator.next();
            }
            this.getStory().add(s);
        }
    }

    /**
     * Input MENU Designs (ASCII)
     *
     * @throws IOException
     */
    public void inputMenus() throws IOException {
        //List<String> lines = Files.readAllLines(Paths.get("Data/Menu.dat"), Charset.defaultCharset());
        List<String> lines = code.decode("Data/Menu.dat");
        String l = "";
        List<String> menus = new ArrayList<>();

        for (String line : lines) {
            if (line.equals("# ======================================== #")) {
                menus.add(l);
                l = "";
            } else {
                l = l + line + "\n";
            }
        }

        //TODO gasaketebelia yvela qalaqis MENU board
        this.loc.get(0).setMenuString(menus.get(0));
        this.loc.get(1).setMenuString(menus.get(1));
        this.loc.get(2).setMenuString(menus.get(1));
        this.loc.get(3).setMenuString(menus.get(2));
        this.loc.get(4).setMenuString(menus.get(2));
        this.loc.get(5).setMenuString(menus.get(3));
        this.loc.get(6).setMenuString(menus.get(3));
        this.loc.get(7).setMenuString(menus.get(4));
        this.loc.get(8).setMenuString(menus.get(4));
        this.loc.get(9).setMenuString(menus.get(5));

    }

    /**
     * Input Random Welcome Messages
     *
     * @throws IOException
     */
    public void inputMessages() throws IOException {
//        List<String> wlcMsgs = Files.readAllLines(Paths.get("Data/wel-msgs.dat"),
//                Charset.defaultCharset());
        List<String> wlcMsgs = code.decode("Data/wel-msgs.dat");
        for (String line : wlcMsgs) {
            this.msgs.add(line);
        }
    }

    /**
    * { QUEST NAME }
    * { INFO / DESCRIPTION }
    * { TASKS }
    * { Awards } @TAGS => gold, xp, town, item
    * { DIFFICULTY }
    * @throws java.io.IOException
    * @throws java.io.FileNotFoundException
    */
    public void inputQuests() throws IOException,FileNotFoundException{
        
        String line, name, info, task, town;
        int diff, xp, gold;
        Map<String, Boolean> tasks;
        List<String> Quests;
        List<Item> questItems;
        
//        try (InputStream fis = new FileInputStream("Data/questsData.dat");
//            InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
//            BufferedReader br = new BufferedReader(isr)){
//            
        
        Quests = code.decode("Data/questsData.dat");

        Iterator<String> read = Quests.iterator();
        while (read.hasNext()) {
            name = read.next();

            if (name.contains("*")) // IGNORE COMMENT
                continue;

            info = read.next();

            xp = 0;
            gold = 0;
            town = "";
            questItems = new ArrayList<>();
            tasks = new HashMap<>();

            task = read.next();
            while(!task.equals("---------------------")){
                tasks.put(task, false);
                task = read.next();
            }
            line = read.next();
            while(!line.equals("---------------------")){
                if (line.contains("xp:")) {
                    xp = Integer.parseInt(line.replace("xp:", ""));
                } else if (line.contains("gold:")) {
                    gold = Integer.parseInt(line.replace("gold:", ""));
                } else if (line.contains("item:")) {
                    String itt = line.replace("item:", "");
                    String[] items = itt.split(" ");
                    for (Item it : this.Items()) {
                        if (it.getName().equals(items[0]) && it.getType() == Types.InvType.valueOf(items[1])) {
                            questItems.add(it);
                            break;
                        }
                    }
                } else if (line.contains("town:")) {
                    town = line.replace("town:", "");
                }
                line = read.next();
            }

            diff = Integer.parseInt(read.next());

            Quest quest = new Quest(name, info, tasks, xp, gold, questItems, town, Types.QuestStatus.Idle, diff);
            this.Quests().add(quest);
        }
    }
    

    public void Save(Character you) {
        File f = new File("Saves");

        if (!f.exists()) {
            f.mkdir();
        }

        String path;
        int filec = 1;
        List<String> files = new ArrayList<>();

        System.out.print("Save file? [Y/n]: ");
        String sav = input.nextLine();
        if (sav.equals("n")) {
            return;
        }
        Funcs.clear();

        System.out.println("Save Files: ");
        System.out.println(filec + " ) Create New File");
        filec += 1;
        
        for (String line : f.list()) {
            line = line.replace(".plr", "");
            files.add(line);
            System.out.println(String.format("%d ) %s", filec, line));
            filec += 1;
        }
        System.out.println(String.format("%d ) back", filec));

        int p_ = Funcs.Input("\n>> ",1,filec);
        if (p_ == 1) {
            System.out.print("Input Name: ");
            path = input.nextLine() + ".plr";
        } else if(p_ == filec){
            return;
        }else{
            path = files.get(p_ - 2) + ".plr";
        }
        
        try(FileOutputStream fos = new FileOutputStream("Saves/" + path);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            PrintWriter out = new PrintWriter(fos);){
            
            // CHAR STATS
            
            oos.writeObject(you);
            out.write("\n");
            
            //out.println(you.toString());

            
            // location firsttime
            for (Location loc : this.Loc()) {
                String ff = String.valueOf(loc.isFirstTime());
                if (loc == this.Locind(this.LocSize() - 1)) {
                    out.println(ff);
                } else {
                    out.print(ff + ",");
                }
            }

            
            // locked location 
            for (Location loc : this.Loc()) {
                String ff = String.valueOf(loc.isLocked());
                if (loc == this.Locind(this.LocSize() - 1)) {
                    out.println(ff);
                } else {
                    out.print(ff + ",");
                }
            }

            // Untold Stories
            for (Story s : this.getStory()) {
                String t = String.valueOf(s.isUntold());
                if (s == this.getStory().get(this.getStory().size() - 1)) {
                    out.println(t);
                } else {
                    out.print(t + ",");
                }
            }

            // current location
            out.println(Base.curLoc);
            
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found" + e.getMessage());
        } catch (IOException ex) {
            System.out.println("Cant read" + ex);
        }

    }

    public void Load() {
        File f = new File("Saves");
        List<String> files = new ArrayList<>();
        String path;
        while (true) {
            int filec = 1;
            Funcs.clear();
            System.out.println("Load Files: ");

            for (String line : f.list()) {
                line = line.replace(".plr", "");
                files.add(line);
                System.out.println(String.format("%d ) %s", filec, line));
                filec += 1;
            }
            System.out.println("--------------------------");
            System.out.println(filec + " ) delete file");
            System.out.println((++filec) + " ) back");

            int p_ = Funcs.Input("\n>> ", 1, filec);
            if (p_ == filec || p_ == 0) {
                return;
            } else if (p_ == filec - 1) {
                int d = Funcs.Input("Input Index of File: ") - 1;
                try {
                    Files.delete(Paths.get("Saves/" + files.get(d) + ".plr"));
                } catch (IOException e) {
                    System.out.println("No such file" + e.getMessage());
                    input.nextLine();//pause
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("Out of Index... try again!");
                    input.nextLine();
                }
            } else {
                path = files.get(p_ - 1) + ".plr";
                break;
            }
        }

        try (InputStream fis = new FileInputStream("Saves/" + path);
            InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
            BufferedReader stream = new BufferedReader(isr);
            ObjectInputStream ois = new ObjectInputStream(fis))
        {

            // Input Char
            Character c = (Character)ois.readObject();

            
            // Reads Location's firstime visit
            stream.readLine();
            String loc = stream.readLine();
            String[] loc_time = loc.split(",");
            for (int i = 0; i < this.LocSize(); i++) {
                this.Locind(i).setFirstTime(Boolean.parseBoolean(loc_time[i]));
            }

            // Reads Location's locked or nah
            loc = stream.readLine();
            String[] lock = loc.split(",");
            for (int i = 0; i < this.LocSize(); i++) {
                this.Locind(i).setLocked(Boolean.parseBoolean(lock[i]));
            }

            // Untold Stories
            String stories = stream.readLine();
            String[] untold = stories.split(",");
            for (int i = 0; i < this.getStory().size(); i++) {
                this.getStory().get(i).setUntold(Boolean.parseBoolean(untold[i]));
            }

            // Current Location
            String curL = stream.readLine();
            Base.curLoc = Integer.parseInt(curL);
            c.setInfo(new String[10]);

            
            this.Chars().add(c); // add to base
            Base.MENU(this);

        } catch (FileNotFoundException e) {
            System.out.println("File Not Found" + e.getMessage());
        } catch (IOException ex) {
            System.out.println("Cant read" + ex);
            input.nextLine();
        } catch (ClassNotFoundException e){
            System.out.println("Class not found");
        }
    }

    /**
     * Formats Menu Design
     *
     * @param m1
     * @param m2
     * @param m3
     * @param m4
     * @return Formated String
     */
    public String menuDesign(Method m1, Method m2, Method m3, Method m4) {
        return String.format(this.loc.get(curLoc).getMenuString(),
                menuStr(m1), menuStr(m2), menuStr(m3), menuStr(m4), "S A V E");//, menuStr(m5));
    }

    public static String menuStr(Method func) {
        String a = "";
        for (int i = 0; i < func.getName().length(); i++) {
            a = a + func.getName().charAt(i) + " ";
        }

        return a.toUpperCase();
    }

    //=-=-=-=-=--=-=-=-=-= M E N U -=-=-=-=-=-=-=-=-=-=
    public static void MENU(Base data) {

        Character Char = null;

        // WHO ARE YOU ?
        for (Character player : data.chars) {
            if (player.isIsPlayer()) {
                Char = player;
            }
        }

        if (Char == null) {
            System.out.println("You aren't Living Creature!");
            return;
        }
        
        

        while (true) {

            Story.printStory(data, Char);

            DateTime now = DateTime.now();

            Map<Integer, Method> MENUFUNCS = new HashMap<>();
//            MENUFUNCS.put(1, data.Locind(curLoc).getFunc().get(0));
//            MENUFUNCS.put(2, data.Locind(curLoc).getFunc().get(1));
//            MENUFUNCS.put(3, data.Locind(curLoc).getFunc().get(2));
//            MENUFUNCS.put(4, data.Locind(curLoc).getFunc().get(3));
            
            //SAME
            for(int i = 0 ; i< 4; i++)
                MENUFUNCS.put(i+1, data.Locind(curLoc).getFunc().get(i));
            
            
            Funcs.messages(Char, data); // print random messages
            System.out.println("Day - " + Char.getTime().getDayOfYear() + " ");// print Day of Year
            System.out.println("\nLocation: " + data.Locind(curLoc).getName());// print Current Location

            //print Menu Board
            System.out.println(data.menuDesign(MENUFUNCS.get(1), MENUFUNCS.get(2), MENUFUNCS.get(3), MENUFUNCS.get(4)));

            int x = Funcs.Input(">> ", 1, 5);
            if (x == 5 || x == 0) {
                boolean ext = true;
                
                data.Save(Char);
                
                OUTER:
                while (ext) {
                    System.out.print("Wanna exit? (Absolutely Yes, I'm Sure! / n): ");
                    String s = input.nextLine();
                    switch (s) {
                        case "Absolutely Yes, I'm Sure!":
                        case "y"://MUAHAHAHAA
                            break OUTER;
                        case "n":
                            ext = false;
                            break;
                    }
                }
                if (ext) {
                    System.exit(0);
                }

            } else {
                try {
                    Method m = MENUFUNCS.get(x);
                    m.setAccessible(true);
                    
                    m.invoke(null, Char, data); // invoke method

                    Funcs.dayCounter(Char, 0, 0, (DateTime.now().getSecondOfDay() - now.getSecondOfDay()) * 20);

                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    System.out.println("You got a big problem");
                    input.nextLine();
                }
            }
            int mints = DateTime.now().getSecondOfDay() - now.getSecondOfDay();

            // Time between 01:00 < - > 06:00
            if (Char.getTime().getHourOfDay() >= 1 && Char.getTime().getHourOfDay() < 6) {
                Funcs.dayCounter(Char, 0, 8, 0); //sleep 8 hours
            }
            Funcs.dayCounter(Char, 0, 0, mints * 10);
        }
    }

}
