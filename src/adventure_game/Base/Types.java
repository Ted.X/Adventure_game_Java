package adventure_game.Base;

import java.io.Serializable;

/**
 *
 * @author ted
 */
public class Types implements Serializable{
    
    public interface setDefault {
        public abstract void setDefaultProperties(Base data, String name);
    }
    
    public enum GearType {
        Weapon1, Weapon2, Head, Chest, Pants, Boots
    }

    // Inventory item type
    public enum InvType {
        NoItem, Sword, Axe, Bow, Shield, Head, Chest, Pants, Boots, Armor, Modifier, QuestItem, Potion, Other
    }
    
    public enum QuestStatus {
        Idle, Active, Passive, Done, Completed
    }
}
