/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.QuestBehaviour;

import adventure_game.Base.*;
import adventure_game.Base.Types.*;
import adventure_game.ItemBehaviour.*;
import adventure_game.Characters.Character;
import java.io.Serializable;
import java.util.*;

/**
 * @author ted
 */
public class Quest implements Serializable{
    /* Gandalf is Coming */

    private String name;
    private String info;
    private Map<String, Boolean> triger;
    private QuestStatus status;
    private int difficulty;
    private int xp;
    private int gold;
    private String town;
    private List<Item> drop;
    private List<String> logs;

    public Quest(String name, String info,
            Map<String, Boolean> triger, 
            int xp, int gold, List<Item> drop, String town, 
            QuestStatus status, int diff) {
        // Info
        this.name = name;
        this.info = info;
        this.town = town;

        // Trigers
        this.triger = triger;

        // Status
        this.status = status;
        this.difficulty = diff;

        // Awards
        this.xp = xp;
        this.gold = gold;
        this.town = town;
        this.drop = drop;
        this.logs = new ArrayList<>();
    }

    public String perc() {
        /* [░░░░░░░░░░░░░░░░░░░░]100 % */
        int countTrue = 0;
        String bar = "";

        for (Map.Entry<String, Boolean> entrySet : getTriger().entrySet()) {
            if (entrySet.getValue()) {
                countTrue++;
            }
        }

        int percent = Math.round(countTrue * 100 / this.getTriger().size());

        for (int i = 0; i < percent / 5; i++) {
            bar += "░";
        }

        return String.format("[%-20s]%d %%", bar, percent);
    }

    public void complete(Character Char, Base data) {

        if (this.getStatus().equals(QuestStatus.Done)) {

            for (Item it : drop) {
                if (!Char.insertItem(it)) {
                    break;
                } else {
                    this.logs.add(String.format("Dropped %s %s", it.getName(), it.getType()));
                }

            }
            int xp = this.getXp() * this.getDifficulty();
            Char.addXp(xp);
            this.logs.add(String.format("Earned %+d XP", xp));

            int gold = Char.getGold() + this.gold;
            Char.setGold(gold);
            this.logs.add(String.format("Earned %+d GOLD", this.gold));

            for (Location l : data.Loc()) {
                if (l.getName().equals(this.getTown())) {
                    l.setLocked(false);
                    this.getLogs().add("Unlocked " + l.getName());
                    break;

                }
            }
            this.logs.add(String.format("Completed on Day - %d", Char.getTime().getDayOfYear()));
            this.setStatus(QuestStatus.Completed);

        }
    }

    public void checkGold(Character Char) {
        for (Map.Entry<String, Boolean> entrySet : getTriger().entrySet()) {
            if (entrySet.getKey().contains("gold")) {
                String gold = entrySet.getKey().replaceAll("[^\\d.]", "");


                if (Char.getGold() >= Integer.parseInt(gold) && !entrySet.getValue()) {
                    entrySet.setValue(true);
                    this.getLogs().add("Collected " + gold + " Gold");
                }
            }
        }
    }

    public void checkXpLvl(Character Char, String x_l) {  // and LVL
        for (Map.Entry<String, Boolean> entrySet : getTriger().entrySet()) {
            if (entrySet.getKey().contains(x_l)) {
                String xorl = entrySet.getKey().replaceAll("[^\\d.]", "");

                try {
                    if (Char.getClass().getSuperclass().getDeclaredField(x_l).getInt(Char) >= Integer.parseInt(xorl)
                            && !entrySet.getValue()) {
                        entrySet.setValue(true);
                        this.getLogs().add(String.format("Reached %s %s", xorl, x_l));
                        //funcs.print_alert("Reached %s %s" % (xorl, x_l))
                    }
                } catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException ex) {
                    System.out.println("Cant Check Quest Tasks");
                    input.nextLine();
                }
            }
        }
    }

    public void checkItem(Character Char) {
        List<Item> allitems = new ArrayList<>();
        allitems.addAll(Arrays.asList(Char.Inv));
        allitems.addAll(Arrays.asList(Char.Gear));
        for (Map.Entry<String, Boolean> entrySet : getTriger().entrySet()) {
            if (entrySet.getKey().contains("Find")) {
                String[] it = entrySet.getKey().replace("Find ", "").split(" ");
                for (Item x : allitems) {
                    if (x.getName().equals(it[0])
                            && x.getType() == Types.InvType.valueOf(it[1])
                            && !entrySet.getValue()) {
                        entrySet.setValue(true);
                        this.getLogs().add("Found " + it[0] + it[1]);
                    }
                }
            }
        }
    }

    public void checkKills(Character Char) {
        for (Map.Entry<String, Boolean> entrySet : getTriger().entrySet()) {
            if (entrySet.getKey().contains("Kill")) {
                String kll = entrySet.getKey().replaceAll("[^\\d.]", "");

                if (Char.getKills() >= Integer.parseInt(kll) && !entrySet.getValue()) {
                    entrySet.setValue(true);
                    this.getLogs().add("Killed " + kll + " men");
                }
            }
        }
    }

    public void checkTown(List<Location> Loc) {
        for (Map.Entry<String, Boolean> entrySet : getTriger().entrySet()) {
            if (entrySet.getKey().contains("Visit")) {
                String lc = entrySet.getKey().replace("Visit ", "");
                for (Location x : Loc) {
                    if (x.getName().equals(lc) && !x.isFirstTime() && !entrySet.getValue()) {
                        entrySet.setValue(true);
                        this.getLogs().add("Visited " + x.getName());
                    }
                }
            }
        }

    }

    public void checkTime(Character Char) {
        // dasamatebelia sikvdilianoba...
        for (Map.Entry<String, Boolean> entrySet : triger.entrySet()) {
            if (entrySet.getKey().contains("Survive")) {
                String d = entrySet.getKey().replaceAll("[^\\d.]", "");
                if (Char.getTime().getDayOfYear() >= Integer.parseInt(d) && !entrySet.getValue()) {
                    entrySet.setValue(true);
                    this.getLogs().add("Survived " + d + " days");
                }
            }

        }
    }


    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return the triger
     */
    public Map<String, Boolean> getTriger() {
        return triger;
    }

    /**
     * @param triger the triger to set
     */
    public void setTriger(Map<String, Boolean> triger) {
        this.triger = triger;
    }

    /**
     * @return the status
     */
    public QuestStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(QuestStatus status) {
        this.status = status;
    }

    /**
     * @return the difficulty
     */
    public int getDifficulty() {
        return difficulty;
    }

    /**
     * @param difficulty the difficulty to set
     */
    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    /**
     * @return the xp
     */
    public int getXp() {
        return xp;
    }

    /**
     * @param xp the xp to set
     */
    public void setXp(int xp) {
        this.xp = xp;
    }

    /**
     * @return the gold
     */
    public int getGold() {
        return gold;
    }

    /**
     * @param gold the gold to set
     */
    public void setGold(int gold) {
        this.gold = gold;
    }

    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @param town the town to set
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     * @return the drop
     */
    public List<Item> getDrop() {
        return drop;
    }

    /**
     * @param drop the drop to set
     */
    public void setDrop(List<Item> drop) {
        this.drop = drop;
    }

    /**
     * @return the logs
     */
    public List<String> getLogs() {
        return logs;
    }

    /**
     * @param logs the logs to set
     */
    public void setLogs(List<String> logs) {
        this.logs = logs;
    }

    static Scanner input = new Scanner(System.in);

    public static void printStatus(Character Char, Base data, String head, QuestStatus St) {
        Map<QuestStatus, String> qfunc = new HashMap<>();
        qfunc.put(QuestStatus.Active, "Deactivate");
        qfunc.put(QuestStatus.Passive, "Activate");
        qfunc.put(QuestStatus.Done, "Collect");
        qfunc.put(QuestStatus.Completed, "View");

        //int ln = 53;
        List<Quest> status = new ArrayList<>();
        for (Quest x : Char.getQuests()) {
            if (x.getStatus() == St) {
                status.add(x);
            }
        }

        if (status.isEmpty()) {
            System.out.println("║                            EMPTY                            ║");//.center(ln)));
        } else {
            for (int i = 0; i < status.size(); i++) {
                System.out.println(String.format("║ @%d %-28s %-28s║", i + 1, (status.get(i).getName() + ":"), status.get(i).perc()));
            }
        }
        System.out.println(String.format("╚═════════════════════════════════════════════ %d or Back ═════╝", (status.size() + 1)));
        int a_ = Funcs.Input(">> ", 1, status.size() + 1);
        if (a_ == 0 || a_ == status.size() + 1)
            return;
        else {
            a_ -= 1;
        }

        Funcs.clear();

        System.out.println(head);


        int ind = 0;
        for (Map.Entry<String, Boolean> entrySet : status.get(a_).getTriger().entrySet()) {
            ind += 1;
            System.out.println(String.format("║ Task %d %-46s %-5s ║",
                    ind, entrySet.getKey(), entrySet.getValue().toString()));
        }
        System.out.println("╚═════════════════════════════════════════════════════════════╝");
        System.out.println("1. " + qfunc.get(St));
        System.out.println("2. Exit");
        int _c = Funcs.Input("\n>> ", 1, 2);
        if (_c == 1) {
            if (St == QuestStatus.Active || St == QuestStatus.Passive) {
                if (qfunc.get(St).equals("Activate")) {
                    int i = 0;
                    for (Quest q : Char.getQuests()) {
                        if (q.getStatus() == QuestStatus.Active)
                            i++;
                    }
                    if (i >= 2) {
                        System.out.println("You Cant!!! Too Many Active Quests.");
                        input.nextLine();
                        return;
                    }
                }
                status.get(a_).setStatus(
                        (St == QuestStatus.Passive) ? QuestStatus.Active : QuestStatus.Passive);

            } else if (St == QuestStatus.Done) {
                status.get(a_).complete(Char, data);
            } else if (St == QuestStatus.Completed) {
                for (String l : status.get(a_).getLogs()) {
                    System.out.println(l);
                }

                input.nextLine();

            }
        }

    }

    public static void QuestsInfo(Character Char, Base data) {
        while (true) {

            int a = 0, p = 0, d = 0, c = 0;

            for (Quest q : Char.getQuests())
                switch (q.getStatus().toString()) {
                    case "Active":
                        a += 1;
                        break;
                    case "Passive":
                        p += 1;
                        break;
                    case "Done":
                        d += 1;
                        break;
                    case "Completed":
                        c += 1;
                        break;
                }

            String head = String.format("╔══════════════╦═══════════════╦════════════╦═════════════════╗\n"
                    + "║ ○ ACTIVE(%2d )║ ○ PASSIVE(%2d )║ ○ DONE(%2d )║ ○ COMPLETED(%2d )║\n"
                    + "╠══════════════╩═══════════════╩════════════╩═════════════════╣", a, p, d, c);

            Funcs.clear();
            System.out.println(head);
            System.out.println("║       v              v              v              v        ║");
            System.out.println("║      |1|            |2|            |3|            |4|       ║");
            System.out.println("╚══════════════════════════════════════════════════════ Quit ═╝");
            int q_ = Funcs.Input(">> ", 1, 4);

            if (q_ == 0)
                return;

            Funcs.clear();
            head = head.replace("○ " + QuestStatus.values()[q_].name().toUpperCase(),
                    "◉ " + QuestStatus.values()[q_].name().toUpperCase());
            System.out.println(head);
            printStatus(Char, data, head, QuestStatus.values()[q_]);
        }
    }

    public static void update(Base data, Character Char) {  // Char.Quest
        for (Quest qst : Char.getQuests()) {
            boolean Done = true;
            if (qst.getStatus() == QuestStatus.Active) {
                qst.checkGold(Char);
                qst.checkXpLvl(Char, "xp");
                qst.checkXpLvl(Char, "lvl");
                qst.checkItem(Char);
                qst.checkKills(Char);
                qst.checkTown(data.Loc());
                qst.checkTime(Char);


                for (Boolean b : qst.getTriger().values()) {
                    if (!b) { // if task is false
                        Done = false;
                        break;
                    }
                }
                if (Done) //&& qst.getStatus() != QuestStatus.Completed) {
                    qst.setStatus(QuestStatus.Done);
            }

        }
    }

    public static String addQuest(Character Char, Base data) {
        List<String> q_names = new ArrayList<>();
        for (Quest x : Char.getQuests()) {
            q_names.add(x.getName());
        }

        for (Quest dtq : data.Quests()) {
            if (!q_names.contains(dtq.name)) {
                dtq.setStatus(QuestStatus.Active);
                Char.getQuests().add(dtq);
                return dtq.getInfo();
            }
        }
        return "False";
    }

    public static void Wizard(Character Char, Base data) {
        String[] wizardMsgs = {"Hello my friend... Good to see you!!!", "Here you are!!!", "Nice to see you again...", "Now what?!", "Where have you been???"};
        Funcs.clear();

        int c = 0;
        for (Quest Quest : Char.getQuests())
            if (Quest.getStatus() == QuestStatus.Active) {
                c++;
            }

        if (c < 2) {
            String info = addQuest(Char, data);
            if (info.equals("False")) {
                Funcs.printCloud("NOPENOPNEOPE", "Wizard", 44, 44);
                return;
            }
            Funcs.printCloud(wizardMsgs[new Random().nextInt(wizardMsgs.length)], "Wizard", 20, 60);
            Funcs.printCloud(info, "Wizard", 44, 44);

        } else {
            Funcs.printCloud("Come back later...", "Wizard", 24, 44);

        }
        Funcs.printCloud("sdfjsikadufhbs dfasdf asdf asd fasd f sad f asd f asdf asd fsajdashjd sadjhas", "Me", 30, 40);
        Funcs.printCloud("Thanks... bye", "Me", 10, 20);
    }
}
