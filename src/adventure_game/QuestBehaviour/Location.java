package adventure_game.QuestBehaviour;

import static java.lang.Math.abs;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;
import java.util.Random;
import adventure_game.Base.*;
import adventure_game.ItemBehaviour.*;
import adventure_game.Characters.Character;

/**
 *
 * @author ted
 */
public class Location {

    private String name;
    private String menuString;
    private List<Method> func;
    private List<Item> shopItems;
    private int price;
    private int currency;
    private boolean firstTime;
    private boolean locked;

    Random rand = new Random();
    
    // Constructors
    public Location(String name, List<Method> func, List<Item> shopItems) {
        this.name = name;
        this.price = 0;
        this.currency = rand.nextInt(100) + 100;
        this.func = func;
        this.shopItems = shopItems;
        locked = true;
        firstTime = true;
    }

    // travel to town
    public void town(Character Char, List<Location> Loc) {
        Scanner input = new Scanner(System.in);
        if (Loc.get(Base.curLoc).name.equals(this.name)) {
            System.out.println("You are here u dumbass!!! :D ");
            input.nextLine();
        } else if (Char.getGold() < this.getPrice()) {
            System.out.println("You don't have enough money to visit this location!!!");
            System.out.print("Press enter to continue...");
            input.nextLine();
        } else {

            this.currency = rand.nextInt(100) + 100; //set new currency???
            int n_town = Loc.indexOf(this);

            Funcs.dayCounter(Char, Math.abs(Base.curLoc - n_town), 0, 0);

            Char.setGold(Char.getGold() - this.getPrice());
            Base.curLoc = Loc.indexOf(this);

        }

    }

    // Choose Town
    public static void travel(Character Char, Base Data) {

        List<Location> loc = new ArrayList<>();

        System.out.println("Choose Destination");
        int i = 0;
        for (Location x : Data.Loc()) {
            if (!x.locked) {
                if(Data.Loc().indexOf(x) == Base.curLoc)
                    x.setPrice(0);
                else{
                    x.setPrice(abs(Base.curLoc - Data.Loc().indexOf(x)) * x.currency);
                }
                System.out.println(String.format("%d. %-20s - cost: %d", (i + 1), x.name, x.price));
                loc.add(x);
                i += 1;
            }
        }
        System.out.println((i + 1) + ". EXIT ");

        int trloc = Funcs.Input(">> ", 1, (i + 1));

        if (trloc == (i + 1) || trloc == 0) {
            return;
        }

        loc.get(trloc - 1).town(Char, Data.Loc());

    }

    /**
     * @return the location name
     */
    public String getName() {
        return name;
    }

    /**
     * @param loc_name the location name to set
     */
    public void setName(String loc_name) {
        this.name = loc_name;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the firstTime
     */
    public boolean isFirstTime() {
        return firstTime;
    }

    /**
     * @param firstTime the firstTime to set
     */
    public void setFirstTime(boolean firstTime) {
        this.firstTime = firstTime;
    }

    /**
     * @return the func
     */
    public List<Method> getFunc() {
        return func;
    }

    /**
     * @param func the func to set
     */
    public void setFunc(List<Method> func) {
        this.func = func;
    }

    /**
     * @return the locked
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     * @param locked the locked to set
     */
    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    /**
     * @return the menuString
     */
    public String getMenuString() {
        return menuString;
    }

    /**
     * @param menuString the menuString to set
     */
    public void setMenuString(String menuString) {
        this.menuString = menuString;
    }

    /**
     * @return the shopItems
     */
    public List<Item> getShopItems() {
        return shopItems;
    }

    /**
     * @param shopItems the shopItems to set
     */
    public void setShopItems(List<Item> shopItems) {
        this.shopItems = shopItems;
    }

}//Location

