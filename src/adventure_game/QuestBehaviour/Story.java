package adventure_game.QuestBehaviour;

import adventure_game.Base.*;
import adventure_game.Characters.Character;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import org.joda.time.DateTime;

public class Story {

    private List<String> storyLine;
    private int line;
    private String loc;
    private String quest;
    private DateTime date;
    private boolean untold;

    public Story() {
        this.storyLine = new ArrayList<>();
        this.line = 0;
        this.untold = true;

    }

    public List<String> getStoryLine() {
        return storyLine;
    }

    public void setStoryLine(List<String> storyLine) {
        this.storyLine = storyLine;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public void addLine() {
        this.line++;
    }

    public boolean isUntold() {
        return untold;
    }

    public void setUntold(boolean untold) {
        this.untold = untold;
    }

    /**
     * @return the loc
     */
    public String getLoc() {
        return loc;
    }

    /**
     * @param loc the loc to set
     */
    public void setLoc(String loc) {
        this.loc = loc;
    }

    /**
     * @return the date
     */
    public DateTime getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(DateTime date) {
        this.date = date;
    }

    /**
     * @return the quest
     */
    public String getQuest() {
        return quest;
    }

    /**
     * @param quest the quest to set
     */
    public void setQuest(String quest) {
        this.quest = quest;
    }

    boolean check(Base data, Character Char) {
        if (this.untold) {
            if (this.date == null || this.date.isBefore(Char.getTime())) {
                if (this.loc == null || data.Locind(Base.curLoc).getName().equals(this.loc)) {
                    if (this.quest == null) {
                        return true;
                    } else {

                        Quest quest = null;
                        for (Quest q : Char.getQuests()) {
                            if (q.getName().equals(this.quest)) {
                                quest = q;
                                break;
                            }
                        }

                        return quest != null && quest.getStatus() == Types.QuestStatus.Completed;

                    }

                }
            }
        }
        return false;
    }

    /**
     * Prints story slowly
     *
     * @param data Base
     * @param Char Character orc elf human
     * @param args Arguments
     */
    public static void printStory(Base data, Character Char, Object... args) {

        double speed = 100;
        
//        uncomment to disable printStory
//        if(1<2)
//            return;
        for (Story story : data.getStory()) {
            if (story.check(data, Char)) {
                List<String> lines = story.getStoryLine();

                //System.out.println("Day - " + story.getDate().getDayOfYear() + "\n"); // print day of year... than story
                if (args.length > 0) { // if method has given arguments for storyline
                    String whole = "";
                    for (String z : lines) { // join as whole string, format and than split it
                        whole = whole.concat(z + "\n");
                    }
                    whole = String.format(whole, (Object[]) args);
                    lines = Arrays.asList(whole.split("\n"));
                }

                try {
                    for (int j = story.getLine(); j < lines.size(); j++) { //print all lines in a single story
                        if (lines.get(j).equals("{break}")) {
                            story.addLine();// save number of line to continue story
                            return;
                        } else if (lines.get(j).length() < 10) { // if line length is less than 10 print without sleep
                            System.out.println(lines.get(j));
                        } else {
                            String[] words = lines.get(j).split(" ");
                            for (String word : words) {
                                for (int i = 0; i < word.length(); i++) {
                                    char c = word.charAt(i);
                                    System.out.print(c);
                                    System.out.flush();
                                    Thread.sleep((long) (0.5 * speed));
                                    if (c == '.') {
                                        Thread.sleep((long) (3 * speed));
                                    } else if (c == ',' || c == '!' || c == '?') {
                                        Thread.sleep((long) (2 * speed));
                                    }
                                }
                                System.out.print(" ");
                            }
                            System.out.println(" ");
                        }
                        story.addLine();
                        Thread.sleep((long) (5 * speed));
                    }
                    story.setUntold(false); //if all lines are printed this story is told now :D
                } catch (InterruptedException e) {
                    System.out.println("time error");
                } catch (NullPointerException e) {
                    System.out.println("NULL" + e);
                }
            }
        }
    }
}
