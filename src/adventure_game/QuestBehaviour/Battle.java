/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.QuestBehaviour;

import adventure_game.Characters.Character;
import adventure_game.Characters.Human;
import adventure_game.Characters.Orc;
import adventure_game.Characters.Elf;

import adventure_game.ItemBehaviour.*;
import adventure_game.Base.Base;
import java.util.Scanner;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import org.joda.time.DateTime;

/**
 *
 * @author ted
 */
public class Battle {

    //++++++++++++++++++++++ B A T T L E +++++++++++++++++++++++++++++
    private static final Random rand = new Random();
    private static final Scanner input = new Scanner(System.in);

    private static DateTime healTime;

    public static void printCharHealth(Character ch1, Character ch2) {
        System.out.println(String.format("%s's health: %d \n", ch2.getName(), (ch2.getHealth() + ch2.getDefense())));
        System.out.println(String.format("%s's health: %d \n", ch1.getName(), (ch1.getHealth() + ch1.getDefense())));
    }

    // if someone's dead ...prints winner
    public static void dead(Character char1, Character char2) {
        char2.setHealth(0);

        char1.addXp(15);//give xp
        //char2.addXp(10);
        char1.setGold(char1.getGold() + rand.nextInt(70) + 30);//give gold
        char1.setKills(char1.getKills()+1);

        System.out.println();
        System.out.print("\t  > WINNER < \n");
        System.out.print("\t.############.\n");
        System.out.print(String.format("\t.#   %-7.10s#.\n", char1.getName()));
        System.out.print("\t.############.\n\n");
        System.out.print("Press enter to continue...");
        input.nextLine();

    }

    //if attacked and how much
    public static boolean attack(Character p1, Character p2) {
        if ((p1.getSpeed() + p1.getStamina()) >= (p2.getSpeed() + p2.getStamina()))//if you are faster
        {
            int hp = p1.hit();//hit power

            int d = p2.getDefense() - hp;
            int h = p2.getHealth() - hp;

            if (p2.getDefense() > 1) {
                p2.setDefense(d);
            } else {
                p2.setHealth(h);
                p2.setDefense(0);
            }

            if (p2.getHealth() < 1) {//if enemy is dead
                dead(p1, p2);
                return true;
            }

            p2.inc_speed();//increase speed
            printCharHealth(p1, p2);
            p1.sub_speed();//decrease speed
            return false;

        } else {
            return attack(p2, p1);
        }

    }

    //same defense
    public static boolean defense(Character p1, Character p2) {
        int hp = p2.hit() / 2;
        int d = p1.getDefense() - hp;
        int h = p1.getHealth() - hp;

        if (p1.getDefense() > 1) {
            p2.setDefense(d);
        } else {
            p1.setHealth(h);
            p1.setDefense(0);
        }


        if (p1.getHealth() < 1) {
            dead(p2, p1);
            return true;
        }
        p1.inc_speed();//increase speed
        p1.inc_speed();//increase speed
        printCharHealth(p1, p2);
        System.out.print("Blocked " + hp + "\n\n");
        p2.sub_speed();
        return false;

    }

    //start battle...
    public static boolean battle(Character char1, Character char2, List<Character> plrs) {
        
        while (true) {

            System.out.println(char1.getName() + " VS " + char2.getName());
            System.out.print("Make your Command!!! \n");
            System.out.print(" 1. ATTACK \n");
            System.out.print(" 2. FULL ATTACK\n");
            System.out.print(" 3. DEFENSE \n");
            System.out.print(" 4. SURRENDER \n");

            int comm = Funcs.Input(">> ", 1, 4);
            Funcs.clear();
            switch (comm) {
                case 1:
                case 2:
                    while (true) {

                        if (attack(char1, char2)) {
                            //char2.setHealth(reset1);
                            //char2.setSpeed(reset2);
                            if (char2.getHealth() < 1) {
                                plrs.remove(char2);
                            }
                            return true;
                        }

                        if (comm == 1) {
                            break;
                        }
                    }
                    break;
                case 3:
                    if (defense(char1, char2)) {
                        if (!char1.isIsPlayer()) {
                            plrs.remove(char1);
                        }
                        return true;
                    }
                    break;
                case 4:
                    System.out.print("RUN YOU FOOL !!!!\n");
                    input.nextLine();
                    return false;

            }
        }

    }

    // battle map... random enemy
    public static Character morph(List<Character> plrs) {

        class Warrior {

            private int x, y; // coordinates
            private final char Wname;
            private final Character player;

            Warrior(Character player, int N, int M) {
                this.Wname = player.isIsPlayer() ? 'Y' : player.getName().charAt(0);
                this.x = rand.nextInt(N - 2) + 1;
                this.y = rand.nextInt(M - 2) + 1;
                this.player = player;
            }

            void set(int N, int M) {

                this.x -= (x > N - 4) ? 1 : (rand.nextInt(3) - 1);
                this.x += (x < 2) ? 1 : (rand.nextInt(3) - 1);

                this.y -= (y > M - 4) ? 1 : (rand.nextInt(3) - 1);
                this.y += (y < 2) ? 1 : (rand.nextInt(3) - 1);

            }

            boolean compareTo(Warrior w1) {
                return this.Wname != w1.Wname && this.x == w1.x && this.y == w1.y;

            }

        }

        Character me = null;
        for (Character x : plrs) {
            if (x.isIsPlayer()) {
                me = x;
                break;
            }
        }
        if (me == null) {
            return null;
        }

        int N = 10 + me.getLvl() * 2;
        int M = 15 + me.getLvl() * 5;
        char[][] v = new char[N + 5][M + 5];

        List<Warrior> w = new ArrayList<>();

        for (Character x : plrs) {
            w.add(new Warrior(x, N, M));
        }

        Character enemy = null;

        //return enemy = X;//morph disabled
        for (int i = 0; i != N; ++i) {
            for (int j = 0; j != M; ++j) {
                if (i == 0 || i == N - 1) {
                    v[i][j] = '=';
                } else if (j == 0 || j == M - 1) {
                    v[i][0] = '║';
                    v[i][M - 1] = '║';
                } else {
                    v[i][j] = ' ';
                }
            }
        }

        while (true) {

            for (Warrior p : w) {
                v[p.x][p.y] = p.Wname;
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                System.out.println(Arrays.toString(ex.getStackTrace()));
            }
            for (int i = 0; i != N; i++) {
                for (int j = 0; j != M; j++) {
                    System.out.print(v[i][j]);
                }
                System.out.println();
            }
            for (int i = 0; i < N; i++) {
                System.out.print("\33[1A");
            }

            for (Warrior p : w) {
                v[p.x][p.y] = ' ';
                p.set(N, M);
            }

            int ind = 0;
            boolean F = true;

            for (int i = 0; i < w.size(); i++) {
                for (Warrior p : w) {
                    if (w.get(ind).compareTo(p)) {
                        enemy = w.get(ind).player;
                        F = false;
                        break;
                    }
                }
                if (!F) {
                    break;
                }
                ind += 1;

            }
            if (enemy != null) {
                break;
            }

        } // End of While

        System.out.print("\033[" + N + "B");
        return enemy;

    }

    // Arena
    public static void arena(Character Char, Base data_) {

        List<Character> plrs = new ArrayList<>();

        // SAVED STATS
        int health = Char.getHealth();
        int defense = Char.getDefense();
        int speed = Char.getSpeed();
        int stamina = Char.getStamina();



        if(Char.getTime().isAfter(healTime)){
            Char.setHealth(Char.getMaxHealth());
            
        }
        if (Char.isHealed()) {
            for (int i = 0; i < Char.getLvl(); i++) {
                int ch = rand.nextInt(3) + 1;
                switch (ch) {
                    case 1:
                        Human h = new Human();
                        //h.setLvl(Char.getLvl() - 1);

                        h.setXp(Char.getXp());
                        h.levelUp();
                        plrs.add(h);
                        break;
                    case 2:
                        Orc o = new Orc();
                        //o.setLvl(Char.getLvl() - 1);
                        o.setXp(Char.getXp());
                        o.levelUp();
                        plrs.add(o);
                        break;
                    case 3:
                        Elf e = new Elf();
                        //e.setLvl(Char.getLvl() - 1);
                        e.setXp(Char.getXp());
                        e.levelUp();
                        plrs.add(e);
                        break;

                }
            }

            plrs.add(Char);

            Scanner read = new Scanner(System.in);
            while (true) {
                Funcs.clear();

                Funcs.printChars(plrs);// print characters properties

                System.out.print("Press any key to continue...Type q to EXIT ARENA \n");
                String exit = read.nextLine();
                if (exit.equals("q") || !battle(Char, morph(plrs), plrs)) {
                    return;
                } else if (plrs.size() < 2 || Char.getHealth() < 1) {
                    break;
                }
                Funcs.dropper(Char, data_.Items());
            }

            if (plrs.get(0).equals(Char)) {
                Funcs.clear();
                System.out.println("           ______________________________________\n"
                                 + "  ________|                                      |_______\n"
                                + "  \\       |     CONGRATS!!! YOU WON ARENA!       |      /\n"
                                + "   \\      |                                      |     /\n"
                                 + "   /      |______________________________________|     \\\n"
                                 + "  /__________)                                (_________\\");
                System.out.println("");
                Char.addXp(100 * Char.getLvl());
                Char.setGold(Char.getGold() + 100 * Char.getLvl());
                read.nextLine();
                
            }
        }
        if(!Char.isHealed()){
            
            if(Char.getTime().isAfter(healTime) || healTime == null)
                healTime = Char.getTime().plusDays(1);
            
            System.out.print("You have to recover until you battle again!\n");
            input.nextLine();
            return;

        }

        // RESET STATS
        Char.setDefense(defense);
        Char.setStamina(stamina);
    }

}
