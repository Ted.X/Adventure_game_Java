/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.Characters;

import adventure_game.ItemBehaviour.*;
import adventure_game.Base.Base;
import adventure_game.Base.Types;
import adventure_game.QuestBehaviour.*;

import java.io.Serializable;
import java.util.*;
import org.joda.time.*; 

/**
 *
 * @author ted
 */
public abstract class Character extends Inventory implements Serializable,Types.setDefault {

    //private static final long serialVersionUID = -698332381524521234L;
    protected String name;
    protected int strength, health, maxHealth, attack, stamina, speed, maxSpeed, defense, xp, max_xp, lvl, gold, kills;
    protected DateTime time;
    protected boolean isPlayer;
    protected List<Quest> Quests;
    transient protected String[] info;

    public Character() {
        super();
        this.attack = 0;
        this.stamina = 0;
        this.defense = 0;
        this.xp = 0;
        this.max_xp = 100;
        this.gold = 100;
        this.lvl = 1;
        this.isPlayer = false;
        this.kills = 0;
        this.invSize = 5;
        this.info = new String[10];
        this.time = new DateTime(1629, 1, 1, 9, 0); // 1629-09-12 09:00:00
        this.Quests = new ArrayList<>();
    }


    Random random = new Random();

    /**
     *
     * @param data
     * @param name
     */
    @Override
    public void setDefaultProperties(Base data, String name){
        this.name = name;
        this.isPlayer = true;
        Quest.addQuest(this, data);
        data.Locind(2).setLocked(false); // Unlock Wizard
    }

    // Set Items's properties
    public void prop(Item x) {
        this.setAttack(this.getAttack() + x.getAttack());
        this.setDefense(this.getDefense() + x.getDefense());
        this.setStamina(this.getStamina() + x.getAgility());
    }

    // delete Item's properties
    public void del_prop(Item y)//delete Items's properties
    {
        this.setAttack(this.getAttack() - y.getAttack());
        this.setDefense(this.getDefense() - y.getDefense());
        this.setStamina(this.getStamina() - y.getAgility());

    }

    @Override
    public String toString() {
        return String.format("%s,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%s,%d,%d",
                this.getName(), this.getStrength(), this.getAttack(),
                this.getHealth(), this.getMaxHealth(), this.getDefense(),
                this.getSpeed(), this.getMaxSpeed(), this.getStamina(),
                this.getXp(), this.getMax_xp(),
                this.getLvl(), this.getGold(),
                this.isIsPlayer(), this.getKills(), this.invSize);
    }

    public void sub_speed() {
        int s = random.nextInt(this.lvl * 70);
        if (this.speed > s) {
            this.setSpeed(this.getSpeed() - s);  // - speed
        }
    }

    public void inc_speed() {
        int s = random.nextInt(this.lvl * 50);
        if (this.speed < this.maxSpeed - s) {
            this.setSpeed(this.getSpeed() + s);  // - speed
        }
    }

    public int hit() {
        int hh;
        if (this.attack < this.strength) {
            hh = random.nextInt(this.strength + 1 - this.attack) + this.attack;
        } else {
            hh = random.nextInt(this.attack + 1 - this.strength) + this.strength;
        }
        System.out.print("Damaged " + hh + " hp -> ");
        return hh;

    }

    public void levelUp() {
        while (this.xp > this.max_xp) {
            this.lvl += 1;
            //this.xp = this.xp - this.max_xp

            if (this.lvl < 2) {
                this.max_xp += this.max_xp / 3;
            } else {
                this.max_xp += this.max_xp * 0.6;
            }

            // Upgrade health and speed
            int str = this.strength * (1 / this.lvl);
            int hlth = this.maxHealth * (1 / this.lvl);
            int spd = this.maxSpeed * (1 / this.lvl);

            this.strength += random.nextInt(this.strength + 1 - str) + str;

            this.maxHealth += random.nextInt(this.maxHealth + 1 - hlth) + hlth;
            this.health = this.maxHealth;

            this.maxSpeed += random.nextInt(this.maxSpeed + 1 - spd) + spd;
            this.speed = this.maxSpeed;

            this.invSize += 2;
        }
    }

    // Add XP
    public boolean addXp(int xx) {
        this.xp += xx;
        if (this.xp >= this.max_xp) {
            this.levelUp();
            return true;
        }
        return false;
    }

    public void goldToxp(int gold) {
        int xp = gold / 3;  // currency :D
        this.gold -= gold;
        Funcs.writeLog("[ DONE ] You Earned " + xp + " Xp");
        this.addXp(xp);
    }

    // Info
    public void updateInfo() {
        this.info[0] = "+++++++++++++++++++++++++++++++";
        this.info[1] = String.format("| Name:      %15s   ", this.name);
        this.info[2] = String.format("| Strength:  %9s  +%3s   ", this.strength, this.attack);
        this.info[3] = String.format("| Health:   (%4s/%4s) +%3s   ", this.health, this.maxHealth, this.defense);
        this.info[4] = String.format("| Speed:    (%4s/%4s) +%3s   ", this.speed, this.maxSpeed, this.stamina);
        this.info[5] = String.format("| Exp:      (%4s/%4s)        ", this.xp, this.max_xp);
        this.info[6] = String.format("| Level:     %-18s", this.lvl);
        this.info[7] = String.format("| Gold:      %-18s", this.gold);
        this.info[8] = String.format("| Kills:     %-18s", this.kills);
        this.info[9] = "+++++++++++++++++++++++++++++++";
    }

    // Print Info
    public void printInfo() {
        for (String i : this.info) {
            if (i.equals(this.info[0]) || i.equals(this.info[9])) // printing table
            {
                System.out.println(i + "+");
            } else {
                System.out.println(i + "|");
            }
        }
    }

    public boolean isHealed() {
        return this.health == this.maxHealth;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the strength
     */
    public int getStrength() {
        return strength;
    }

    /**
     * @param strength the strength to set
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * @return the health
     */
    public int getHealth() {
        return health;
    }

    /**
     * @param health the health to set
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * @return the health
     */
    public int getMaxHealth() {
        return maxHealth;
    }

    /**
     * @param health the health to set
     */
    public void setMaxHealth(int health) {
        this.maxHealth = health;
    }

    /**
     * @return the attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * @param attack the attack to set
     */
    public void setAttack(int attack) {
        this.attack = attack;
    }

    /**
     * @return the stamina
     */
    public int getStamina() {
        return stamina;
    }

    /**
     * @param stamina the stamina to set
     */
    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    /**
     * @return the speed
     */
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return the speed
     */
    public int getMaxSpeed() {
        return maxSpeed;
    }

    /**
     * @param speed the speed to set
     */
    public void setMaxSpeed(int speed) {
        this.maxSpeed = speed;
    }

    /**
     * @return the defense
     */
    public int getDefense() {
        return defense;
    }

    /**
     * @param defense the defense to set
     */
    public void setDefense(int defense) {
        this.defense = defense;
    }

    /**
     * @return the xp
     */
    public int getXp() {
        return xp;
    }

    /**
     * @param xp the xp to set
     */
    public void setXp(int xp) {
        this.xp = xp;
    }

    /**
     * @return the max_xp to set
     */
    public int getMax_xp() {
        return max_xp;
    }

    /**
     * @param max_xp the max_xp to set
     */
    public void setMax_xp(int max_xp) {
        this.max_xp = max_xp;
    }

    /**
     * @return the lvl
     */
    public int getLvl() {
        return lvl;
    }

    /**
     * @param lvl the lvl to set
     */
    public void setLvl(int lvl) {
        this.lvl = lvl;
    }

    /**
     * @return the gold
     */
    public int getGold() {
        return gold;
    }

    /**
     * @param gold the gold to set
     */
    public void setGold(int gold) {
        this.gold = gold;
    }

    /**
     * @return the kills
     */
    public int getKills() {
        return kills;
    }

    /**
     * @param kills the kills to set
     */
    public void setKills(int kills) {
        this.kills = kills;
    }

    /**
     * @return the isPlayer
     */
    public boolean isIsPlayer() {
        return isPlayer;
    }

    /**
     * @param isPlayer the isPlayer to set
     */
    public void setIsPlayer(boolean isPlayer) {
        this.isPlayer = isPlayer;
    }

    /**
     * @return the info
     */
    public String[] getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String[] info) {
        this.info = info;
    }

    /**
     * @return the Quests
     */
    public List<Quest> getQuests() {
        return Quests;
    }

    /**
     * @param Quests the Quests to set
     */
    public void setQuests(List<Quest> Quests) {
        this.Quests = Quests;
    }

    /**
     * @return the time
     */
    public DateTime getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(DateTime time) {
        this.time = time;
    }

}
