/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.Characters;

import adventure_game.Base.Base;


/**
 *
 * @author ted
 */
public class Elf extends Character {

    public Elf() {
        super();
        this.name = "Elf";

        this.strength = 20 + this.attack;
        this.speed = 130 + this.stamina;
        this.maxSpeed = this.speed;
        this.health = 100 + this.defense;
        this.maxHealth = this.health;

        this.gold = 300;
        this.updateInfo();
    }

    @Override
    public void setDefaultProperties(Base data, String name) {
        super.setDefaultProperties(data, name);
        this.Gear[0] = data.Items().get(2);
        this.Gear[1] = data.Items().get(6);
        prop(data.Items().get(2));
        prop(data.Items().get(6));
        data.Loc().get(4).setLocked(false);  // Unlock Golden Forest
        Base.curLoc = 4;
    }

    

}