/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.Characters;

import adventure_game.Base.Base;


/**
 *
 * @author ted
 */
public class Human extends Character {

    public Human() {
        super();
        this.name = "Human";

        this.strength = 15 + this.attack;
        this.speed = 130 + this.stamina;
        this.maxSpeed = this.speed;
        this.health = 50 + this.defense;
        this.maxHealth = this.health;

        this.updateInfo();
    }

    @Override
    public void setDefaultProperties(Base data, String name) {
        super.setDefaultProperties(data, name);
        this.Gear[0] = data.Items().get(1);
        this.Gear[1] = data.Items().get(7);

        prop(data.Items().get(1));
        prop(data.Items().get(7));
        data.Loc().get(0).setLocked(false);  // Unlock Nemertea
        Base.curLoc = 0;
    }
}
