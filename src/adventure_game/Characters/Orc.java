/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adventure_game.Characters;

import adventure_game.Base.Base;

/**
 *
 * @author ted
 */
public class Orc extends Character{

    public Orc() {
        super();
        this.name = "Orc";

        this.strength = 25 + this.attack;
        this.speed = 150 + this.stamina;
        this.maxSpeed = this.speed;
        this.health = 40 + this.defense;
        this.maxHealth = this.health;
        
        this.updateInfo();
    }

    @Override
    public void setDefaultProperties(Base data, String name) {
        super.setDefaultProperties(data, name);
        this.Gear[0] = data.Items().get(7);
        prop(data.Items().get(7));
        data.Loc().get(6).setLocked(false);  // Unlock Orc's Stronghold
        Base.curLoc = 6;
    }
}
